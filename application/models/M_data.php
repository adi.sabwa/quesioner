<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_data extends CI_Model {

	private $table = 'data';

	public function get($id) {
		return $this->db->where('id',$id)
						->get($this->table)
						->row();
	}

	public function insert($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function update_by_nomor_id($answer,$nomor,$id)
	{
		$data['no_'.$nomor] = $answer;

		return $this->db->where('id',$id)
						->update($this->table, $data);
	}

	public function delete($id)
	{
		return $this->db->where('id',$id)
						->update($this->table);
	}
}