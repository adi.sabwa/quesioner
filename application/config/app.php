<?php 

$config['hadiah'] = [
    'A' => [ '1' => 65000, '2' => 52000],
    'B' => [ '1' => 125125, '2' => 3250],
];

$config['probabilitas'] = [
    '1' => [ '1' => 10, '2' => 90],
    '2' => [ '1' => 20, '2' => 80],
    '3' => [ '1' => 30, '2' => 70],
    '4' => [ '1' => 40, '2' => 60],
    '5' => [ '1' => 50, '2' => 50],
    '6' => [ '1' => 60, '2' => 40],
    '7' => [ '1' => 70, '2' => 30],
    '8' => [ '1' => 80, '2' => 20],
    '9' => [ '1' => 90, '2' => 10],
    '10' => [ '1' => 100, '2' => 0],
];

$config['tipe'] = [
    0 => 'Highly Risk Loving (Amat Sanget Menyukai Risiko)',
    1 => 'Highly Risk Loving (Amat Sanget Menyukai Risiko)',
    2 => 'Very Risk Loving (Sanget Menyukai Risiko)',
    3 => 'Risk Loving (Menyukai Risiko)',
    4 => 'Risk Neutral (Tidak Menyukai/Menghindari Risiko)',
    5 => 'Slightly Risk Averse (Agak Menghindari Risiko)',
    6 => 'Risk Averse (Menghindari Risiko)',
    7 => 'Very Risk Averse (Sanget Menghindari Risiko)',
    8 => 'Highy Risk Averse (Amat Sanget Menghindari Risiko)',
    9 => 'Stay in Bed',
    10 => 'Stay in Bed',
];