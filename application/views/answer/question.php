<form action="<?= site_url()?>/answer/answer" method="post">
	<?php for ($nomor=1; $nomor <= 10; $nomor++) { ?>
	<div class="warp-question" >
		<h3>Kondisi ke <?= $nomor ?></h3>
		<div class="question">Lotre mana yang Anda pilih ? </div>
		<div class="answer">
			<input type="hidden" name="nomor" value="<?= $nomor ?>"></input>
			<input type="radio" id="answer<?=$nomor?>" name="answer<?=$nomor?>" value="A" style="margin-top: 5px;" <?php echo  set_radio('answer'.$nomor,'A'); ?> >&nbsp; A. &nbsp;</input>
			<div class="choice">
				Rp <?= number_format($hadiah['A']['1'],0,',','.') ?> dengan peluang sebesar <?= $peluang[$nomor]['1']?> %<br/>
				Rp <?= number_format($hadiah['A']['2'],0,',','.') ?> dengan peluang sebesar <?= $peluang[$nomor]['2']?> %
			</div>
			<input type="radio" id="answer<?=$nomor?>" name="answer<?=$nomor?>" value="B" style="margin-top: 5px;"  <?php echo  set_radio('answer'.$nomor,'B'); ?> >&nbsp; B. &nbsp;</input>
			<div class="choice">
				Rp <?= number_format($hadiah['B']['1'],0,',','.') ?>  dengan peluang sebesar <?= $peluang[$nomor]['1']?> %<br/>
				Rp <?= number_format($hadiah['B']['2'],0,',','.') ?>  dengan peluang sebesar <?= $peluang[$nomor]['2']?> %
			</div>
		</div>
		 <div style="color: red;"><?php echo form_error('answer'.$nomor); ?></div>
	</div>
	<?php } ?>
	<div style="margin-top: 10px;">
		<button class="btn btn-success" style="margin-left: 60vw;">Submit</button>
	</div>
</form>