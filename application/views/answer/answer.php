<div class="warp-question">
	<h3>Jawaban Pertanyaan ke <?= $nomor ?></h3>
	<div class="question">Lotre yang Anda Pilih </div>
	<div class="answer">
		<div class="choice-wrap <?= $jawaban->jawaban == 'A' ? 'choice-answer' : '' ?>">A.&nbsp;
		<div class="choice">
			Rp <?= number_format($hadiah['A']['1'],0,',','.') ?> dengan peluang sebesar <?= $peluang['1']?> %<br/>
			Rp <?= number_format($hadiah['A']['2'],0,',','.') ?> dengan peluang sebesar <?= $peluang['2']?> %
		</div>
		</div>
		<div class="choice-wrap <?= $jawaban->jawaban == 'B' ? 'choice-answer' : '' ?>">B.&nbsp;
		<div class="choice">
			Rp <?= number_format($hadiah['B']['1'],0,',','.') ?>  dengan peluang sebesar <?= $peluang['1']?> %<br/>
			Rp <?= number_format($hadiah['B']['2'],0,',','.') ?>  dengan peluang sebesar <?= $peluang['2']?> %
		</div>
		</div>
	</div>
	<div class="question"><b>Hasil Undian : </b></div>
	<div class="question">Hadiah yang Anda Dapatkan </div>
	<div>
		<i><b>Rp <?= number_format($jawaban->undian,2,',','.') ?></b></i>
	</div>
	<div style="margin-top: 10px;">
		<?php if ($nomor < 10) {?>
		<a class="btn btn-success" href="<?= site_url() ?>/answer?nomor=<?=$nomor+1?>">Pertanyaan Selanjutnya &nbsp;<img src="<?= base_url()?>/assets/images/caret-left.png" height="10"/> </a>
		<?php } else {?>
		<a class="btn btn-success" href="<?= site_url() ?>/welcome/hasil">Hasil &nbsp;<img src="<?= base_url()?>/assets/images/caret-left.png" height="10"/> </a>
		<?php }?>
	</div>
</div>