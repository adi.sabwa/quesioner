<!DOCTYPE html>
<html lang="en">
<head>
  <?php 
  $updated     = strtotime('2019-09-27 11:00:00');
  $js_version  = '?v='.$updated;
  $css_version = $js_version;
  ?>
	<meta charset="utf-8">
	<title>Kuesioner Penelitian</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="site-url" content="<?= site_url() ?>">
  <meta name="base-url" content="<?= base_url() ?>">
  	<link rel="icon" href="<?= base_url('assets/images/favicon.ico') ?>">
  	<link rel="stylesheet" href="<?= base_url('assets/css/custom.css'.$css_version) ?>">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<div id="container">
	<h1 style="text-align: center;">Eksperimen Holt <i>and</i> Laury <i>Lottery</i></h1>
	<div id="body">
		<div class="menu">
			<a href="<?= site_url() ?>/welcome">Home</a> &nbsp; &nbsp; &nbsp; &nbsp;
		</div>
		<div class="content">
			<?= $content ?>
		</div>
		
		<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
	</div>

</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>