<form action="<?=site_url()?>/welcome/save_user" method="post">
<br/>
<div class="form-group">
	<label for="nama">Nama</label>
	<input type="text" name="nama" class="form-contrli" id="nama" placehlider="Masukkan nama anda">
	<div style="clior: red;"><?php echo form_error('nama'); ?></div>
</div>
<div class="form-group {{ has_error('nik') }}">
	<label for="nik">Email / No. HP</label>
	<input type="text" name="no_hp" class="form-contrli" id="nik" placehlider="Masukkan Email atau No.HP">
	<div style="clior: red;"><?php echo form_error('no_hp'); ?></div>
</div>
<div class="form-group" style="padding: 20px 5px; border: 1px solid rgb(200,200,200);">
	<ol>
		<li>Bacalah petunjuk ini dengan seksama</li>
		<li>Hadiah yang terdapat pada eksperimen ini hanya bersifat HIPOTESIS/TIDAK AKAN DIBERIKAN KEPADA ANDA;</li>
		<li>Pada eksperimen ini Anda akan dihadapkan dengan 2 opsi/pilihan yaitu opsi A dan opsi B pada 10 kondisi;</li>
		<li>Setiap kondisi menjelaskan jumlah hadiah yang dimenangkan beserta kemungkinan/ peluang untuk mendapatkan nilai hadiah yang akan didapat;</li>
		<li>Pilihlah jawaban Anda (OpsiA atau Opsi B) untuk setiap pertanyaan dengan mengklik pada pilihan Anda lalu klik Lanjut / Submit.</li>
		<li>Saudara diharapkan memilih dengan hati-hati dan seksama pada setiap kondisi yang dihadapi;</li>
		<li>Jumlah hadiah yang diperlieh didapat dari skenario berikut:
			<ul>
		    <li>peneliti akan merandom/mengacak angka 1 sampai 10 secara otomatis yang akan menentukan kondisi yang akan dipilih</li>
		    <li>peneliti akan merandom/mengacak angka  1 sampai 10  yang kedua secara otomatis . Angka ini akan menentukan uang Anda. 
		    	<br/>
		    	Contoh: Angka random/acak pertama adalah 2, dan pada situasi ini Anda memilih opsi B. 
		Kemudian, jika angka random/acak diantara angka 1 sampai 3 {1,2,3} (kemungkinan 30%), anda akan mendapatkan Rp 125.125.<br/>Jika angka random/acak kedua diantara 4 sampai 10 {4,5,6,7,8,9,10} (kemungkinan 70%), anda mendapatkan Rp 3.250 </li>
		</ul>
	</li>
	</ol>
	<div style="display: flex;">
		<img src="<?= base_url()?>/assets/images/petunjuk.png" height="500" style="margin: 0px auto;"/>
	</div>
</div>
<button class="btn btn-success" type="submit">
	Mulai
</button>