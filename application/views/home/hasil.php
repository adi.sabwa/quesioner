<br/>
<div class="form-group">
	<label>Nama : </label>
	<label><b><?= $nama?></b></label>
</div>
<div class="form-group">
	<label>Kondisi yang terplih : </label>
	<label><b>Kondisi ke-<?= $situasi?></b></label>
</div>

<div class="form-group">
	<label>Lotre yang dipilih : </label>
	<label><b><?= $jawaban->jawaban?></b></label>
</div>

<div class="form-group">
	<label>Hadiah yang didapat : </label>
	<label><b>Rp. <?= number_format($jawaban->undian,0,',','.') ?> </b></label>
</div>

<div class="form-group">
	<label>Hasil : </label>
	<label>Anda termasuk orang yang <b><?= $tipe?></b></label>
</div>