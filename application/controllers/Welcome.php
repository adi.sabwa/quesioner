<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('m_data');

	}

	public function index()
	{
		return view('home/index');
	}

	public function save_user()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('no_hp', 'Email atau Nomor HP', 'required');

		if ($this->form_validation->run()) {
			$data = [
				'nama' => $this->input->post('nama'),
				'no_hp' => $this->input->post('no_hp'),
			];

			$save = $this->m_data->insert($data);
			$data['id'] = $this->db->insert_id();
			set_userdata($data);
			redirect('answer');
		} else {
			$this->index();
		}
	}
}
