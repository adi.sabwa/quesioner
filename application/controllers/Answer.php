<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Answer extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('m_data');

	}

	public function index($nomor = NULL)
	{
		// $nomor = empty($nomor) ? $this->input->get('nomor') : $nomor;
		// $nomor = empty($nomor) ? '1' : $nomor;
		$hadiah = $this->config->item('hadiah');
		$probabilitas = $this->config->item('probabilitas');
		$peluang = $probabilitas;
		return view('answer/question',compact('nomor','hadiah','peluang'));
	}

	public function answer()
	{
		for ($i=1; $i <=10 ; $i++) { 
			$this->form_validation->set_rules('answer'.$i, 'Jawaban', 'required');
			// var_dump($this->input->post('answer'.$i));
		}
		// exit;
		if ($this->form_validation->run()) {
			for ($i=1; $i <=10 ; $i++) { 
				$answer = $this->input->post('answer'.$i);
				$save = $this->m_data->update_by_nomor_id($answer,$i,userdata()['id']);
			}
			redirect('answer/hasil');
		} else {
			$this->index();
		}
	}
	
	public function hasil()
	{
		$user = $this->m_data->get(userdata()['id']);
		$safe = 0;
		for ($i=1; $i < 11; $i++) { 
			$att = 'no_'.$i;
			if($user->$att == 'A') $safe++;
		}

		$data['nama'] = $user->nama;
		$data['no_hp'] = $user->no_hp;
		$data['tipe'] = $this->config->item('tipe')[$safe.''];

		$situasi = rand(1,10);
		$probabilitas = $this->config->item('probabilitas');
		$peluang = $probabilitas[$situasi];
		$probabilty = $this->check_by_probability($peluang['1']);

		$hadiah = $this->config->item('hadiah');
		$jawaban = new StdClass;
		$att = 'no_'.$situasi;
		$answer = $user->$att;
		$jawaban->jawaban = $answer;
		$jawaban->undian = $probabilty ? $hadiah[$answer]['1'] : $hadiah[$answer]['2'];
		$data['situasi'] = $situasi;
		$data['jawaban'] = $jawaban;
		return view('home/hasil',$data);
	}

	public function hasil_2()
	{

		$this->form_validation->set_rules('answer', 'Jawaban', 'required');
		$nomor = $this->input->post('nomor');

		if ($this->form_validation->run()) {
			$answer = $this->input->post('answer');

			$hadiah = $this->config->item('hadiah');
			$probabilitas = $this->config->item('probabilitas');
			$peluang = $probabilitas[$nomor];
			$probabilty = $this->check_by_probability($peluang['1']);
			$jawaban = new StdClass;
			$jawaban->jawaban = $answer;
			$jawaban->undian = $probabilty ? $hadiah[$answer]['1'] : $hadiah[$answer]['2'];
			$save = $this->m_data->update_by_nomor_id($answer,$nomor,userdata()['id']);
			return view('answer/answer',compact('nomor','hadiah','peluang','jawaban'));
		} else {
			$this->index($nomor);
		}
	}

	private function check_by_probability($prob = 50)
	{
		$number = rand(1,100);
		if ($number <= $prob) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}