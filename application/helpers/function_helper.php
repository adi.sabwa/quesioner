<?php

if ( ! function_exists('view')) 
{
	function view($path, $_data = NULL)
	{
		$ci =& get_instance();		
	    $data['content'] = $ci->load->view($path, $_data, TRUE);
		$ci->load->view('template',$data);
	}
}

if ( ! function_exists('format_to_indo')) 
{
    function format_to_indo($time, $input = 'time', $day_name = false)
    {
        if ($input == 'date') {
            $time .= ' 00:00:00';
            $time  = strtotime($time);
        }

        $name  = date('N', $time);
        $day   = date('j', $time);
        $month = date('n', $time);
        $year  = date('Y', $time);

        if ($day_name) {
            return unserialize(DAY_LIST)[$name].', '.$day.' '.unserialize(MONTH_LIST)[$month].' '.$year;
        }
        return $day.' '.unserialize(MONTH_LIST)[$month].' '.$year;
    }
}